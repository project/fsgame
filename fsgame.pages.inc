<?php

/**
 * @file
 * Page callbacks for the 5 Second Game engine.
 */

/**
 *  Page callback for /fsgame/contest.
 *  Allow two characters to compete.
 *  @param $form_state
 *    The form state.
 *  @param $character_nid
 *    The character nid, or a string to match against fsgame_contest_match_character.
 *  @param $opponent_nid
 *    The opponent nid, or a string to match against fsgame_contest_match_opponent.
 */
function fsgame_contest_form($form_state, $character_nid = 'latest', $opponent_nid = 'latest') {
  global $user;
  $form = array();
  $form['character'] = array(
    '#type' => 'fieldset',
    '#title' => t('Your character'),
    '#collapsible' => TRUE,
  );
  if ($user->uid) {
    $cnid = fsgame_contest_match_character($character_nid, $opponent_nid);
  }
  else {
    $cnid = fsgame_contest_anonymous_character();
  }
  if ($cnid && is_numeric($cnid)) {
    $character = node_load($cnid);
    $form['character']['#title'] = t('Your character: @name', array('@name' => $character->title));
    $form['character']['#collapsed'] = TRUE;
    $form['character']['character_node'] = array(
      '#type' => 'value',
      '#value' => $character,
    );
    $form['character']['teaser'] = array(
      '#type' => 'item',
      '#value' => node_view($character),
    );
    fsgame_user_set_latest_character($user->uid, $character->nid);
  }
  else {
    $cnid = 'latest';
  }
  $form['character']['character_autocomplete'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#autocomplete_path' => 'fsgame/autocomplete/'. $user->uid,
  );
  $form['character']['select'] = array(
    '#type' => 'submit',
    '#value' => t('Select character'),
  );
  $form['opponent'] = array(
    '#type' => 'fieldset',
    '#title' => t('Your opponent'),
    '#collapsible' => TRUE,
  );
  $onid = fsgame_contest_match_opponent($opponent_nid, $cnid);
  if ($onid && is_numeric($onid)) {
    $opponent = node_load($onid);
    $form['opponent']['#title'] = t('Your opponent: @name', array('@name' => $opponent->title));
    $form['opponent']['#collapsed'] = TRUE;
    $form['opponent']['opponent_node'] = array(
      '#type' => 'value',
      '#value' => $opponent,
    );
    $form['opponent']['teaser'] = array(
      '#type' => 'item',
      '#value' => node_view($opponent),
    );
  }
  else {
    $onid = 'latest';
  }

  // If we don't allow contests of characters of the same user, then exclude them from the autocomplete.
  $opponent_autocomplete_exclude_path = variable_get('fsgame_contest_allow_same_user', FALSE) ? '' : '-exclude';
  $form['opponent']['opponent_autocomplete'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#autocomplete_path' => 'fsgame/autocomplete'. $opponent_autocomplete_exclude_path .'/'. $user->uid,
  );

  $form['opponent']['select'] = array(
    '#type' => 'submit',
    '#value' => t('Select opponent'),
  );
  $form['character']['selectors'] = array(
    '#type' => 'item',
    '#value' => theme('fsgame_contest_selectors_character', $cnid, $onid),
  );
  $form['opponent']['selectors'] = array(
    '#type' => 'item',
    '#value' => theme('fsgame_contest_selectors_opponent', $cnid, $onid),
  );
  $form['modes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Contest modes'),
  );

  $attributes = fsgame_base_attributes();
  foreach ($attributes as $key => $attribute) {
    $attacks[$key] = $attribute['title'];
    $defenses[$key] = $attribute['title'];
  }
  // Allow modules implementing hook_fsgame_available_modes_alter to add/remove their own skills to/from the selectors.
  drupal_alter('fsgame_available_modes', $attack, 'character', 'attack', $character, $opponent);
  drupal_alter('fsgame_available_modes', $defense, 'character', 'defense', $character, $opponent);

  $form['modes']['attack'] = array(
    '#type' => 'select',
    '#title' => t('Attack'),
    '#options' => $attacks,
  );
  $form['modes']['defense'] = array(
    '#type' => 'select',
    '#title' => t('Defense'),
    '#options' => $defenses,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Fight!'),
  );
  return $form;
}

function fsgame_contest_form_validate($form, &$form_state) {
  global $user;
  $contest = array();
  $character = $form_state['values']['character_node'];
  if ($form_state['values']['character_autocomplete'] || !($form_state['values']['character_node'])) {
    $character = node_load(_fsgame_autocomplete_nid($form_state['values']['character_autocomplete']));
    if (($form_state['values']['op'] == t('Fight!')) && ($character->type != 'fsgame_character') || ($character->uid != $user->uid)) {
      form_set_error('character_autocomplete', t('Please select a valid character that you own.'));
    }
  }
  $opponent = $form_state['values']['opponent_node'];
  if ($form_state['values']['opponent_autocomplete'] || !($form_state['values']['opponent_node'])) {
    $opponent = node_load(_fsgame_autocomplete_nid($form_state['values']['opponent_autocomplete']));
    if (($form_state['values']['op'] == t('Fight!')) && $opponent->type != 'fsgame_character') {
      form_set_error('opponent_autocomplete', t('Please select a valid opponent.'));
    }
  }
  $contest['character'] = $character;
  $contest['opponent'] = $opponent;
  if (($form_state['values']['op'] == t('Fight!')) && !fsgame_contest_allow($contest)) {
    // print the reason returned for not allowing a specific contest.
    form_set_error('', $contest['results']['disallow']);
  }
  $form_state['values']['contest'] = $contest;
}

function fsgame_contest_form_submit($form, &$form_state) {
  global $user;
  if ($form_state['values']['character_autocomplete']) {
    $form_state['values']['character_node'] = node_load(_fsgame_autocomplete_nid($form_state['values']['character_autocomplete']));
  }
  if ($form_state['values']['opponent_autocomplete']) {
    $form_state['values']['opponent_node'] = node_load(_fsgame_autocomplete_nid($form_state['values']['opponent_autocomplete']));
  }
  fsgame_user_set_latest_character($user->uid, $form_state['values']['character_node']->nid);
  if ($form_state['values']['op'] == t('Fight!')) {
    $contest = $form_state['values']['contest'];
    $contest['character attack'] = fsgame_contest_select_mode('attack', $form_state['values']['attack'], $form_state['values']['character_node'], $form_state['values']['opponent_node']);
    $contest['character defense'] = fsgame_contest_select_mode('defense', $form_state['values']['defense'], $form_state['values']['character_node'], $form_state['values']['opponent_node']);
    fsgame_contest($contest);
    $winner = $contest['results']['win'];
    if ($winner == 'draw') {
      drupal_set_message(t('The result is a draw.'));
    }
    else if ($winner->nid) {
      drupal_set_message(t('!title wins the contest.', array('!title' => l($winner->title, 'node/'. $winner->nid))));
    }
    else {
      drupal_set_message(t('Unknown error in fsgame_contest_form_submit'), 'error');
    }
  }
  drupal_goto('fsgame/contest/'. $form_state['values']['character_node']->nid .'/'. $form_state['values']['opponent_node']->nid);
}

/**
 *  Match a game character.
 *  @param $character_nid
 *  Either the nid of the character to compete with, or one of the following strings:
 *      'latest' => The last selected character of the currently logged in user.
 *      'newest' => The most recently created character of the currently logged in user.
 *      'random' => A random character from among those owned by the currently logged in user.
 *      'select' => A drop-down of characters owned by the currently logged in user.
 *      'autocomplete' => An autocomplete textfield, otherwise the same as 'select'.
 *      'anonymous' => The anonymous character stored in the user's session (reverts to 'select' if not anonymous).
 *    If the user is anonymous, then this parameter is ignored, and treated as though it were 'anonymous'.
 *    Otherwise, if this doesn't match, then the string from $character_nid is passed through hook_fsgame_contest_match_character,
 *    so that other modules may choose a character from the given argument.
 *  @TODO: finish implementing this
 */
function fsgame_contest_match_character($character_nid, $opponent_nid = NULL) {
  global $user;
  if ($character_nid && is_numeric($character_nid)) {
    $node = node_load($character_nid);
    if ($node->type == 'fsgame_character' && ($node->uid == $user->uid)) {
      return $character_nid;
    }
    return FALSE;
  }
  if ($character_nid == 'latest') {
    return fsgame_user_get_latest_character($user->uid);
  }
  if ($character_nid == 'newest') {
    $sql = "SELECT nid FROM {node} WHERE type='fsgame_character' AND uid=%d ORDER BY created DESC";
    return db_result(db_query_range($sql, $user->uid, 0, 1));
  }
  if ($character_nid == 'random') {
    $sql = "SELECT nid FROM {node} WHERE type='fsgame_character' AND uid=%d ORDER BY RAND()";
    drupal_goto('fsgame/contest/'. db_result(db_query_range($sql, $user->uid, 0, 1)) .'/'. $opponent_nid);
  }
}

/**
 *  Match a game opponent character.
 *  @param $opponent_nid
 *    Either the nid of the character to compete against, or one of the following strings:
 *      'latest' => The last selected opponent of the selected character.
 *      'random' => A random suitable opponent.
 *      'select' => A drop-down of suitable opponents.
 *      'autocomplete' => An autocomplete textfield, otherwise the same as 'select'.
 *      'user:%uid' => A random suitable opponent belonging to the specified %uid.
 *    If this doesn't match, then the string from $opponent_nid is passed through hook_fsgame_contest_match_opponent,
 *    so that other modules may choose an opponent from the given argument.
 *  @param $character_nid
 *    If provided, this is the currently matched character's node's NID.
 */
function fsgame_contest_match_opponent($opponent_nid, $character_nid = NULL) {
  global $user;
  if ($opponent_nid && is_numeric($opponent_nid)) {
    $node = node_load($opponent_nid);
    if ($node->type == 'fsgame_character' && ($node->uid != $user->uid)) {
      return $opponent_nid;
    }
    return FALSE;
  }
  if ($opponent_nid == 'latest' && $character_nid && is_numeric($character_nid)) {
    return fsgame_character_get_latest_opponent($character_nid);
  }
  if ($opponent_nid == 'random') {
    $sql = "SELECT nid FROM {node} WHERE type='fsgame_character' AND uid<>%d ORDER BY RAND()";
    $character_nid = isset($character_nid) ? $character_nid : '/latest';
    return drupal_goto('fsgame/contest/'. $character_nid .'/'. db_result(db_query_range($sql, $user->uid, 0, 1)));
  }
}

/**
 *  Return the character from this anonymous user's session.
 *  @TODO
 */
function fsgame_contest_anonymous_character() {
  return $_SESSION['fsgame_contest_anonymous_character'];
}

/**
 * Retrieve a pipe delimited string of autocomplete suggestions
 *  @param $uid
 *  If a number that is not 0, then this will retrieve only characters of that user account.
 *  @param $string
 *  A partial string to match.
 *  @param $exclude_uid
 *  If non-NULL, then it will exclude characters of this user, including anonymous.
 */
function fsgame_autocomplete($uid = NULL, $string = '') {
  $matches = array();

  $nodes = _fsgame_potential_nodes($uid, TRUE, $string);
  foreach ($nodes as $node) {
    $matches[check_plain($node->title) .' ('. $node->name .') ['. $node->nid .']'] = check_plain($node->title) .' ('. $node->name .') ['. $node->nid .']';
  }
  drupal_json($matches);
}

/**
 * Retrieve a pipe delimited string of autocomplete suggestions.
 *  @param $uid
 *  If non-NULL, then it will exclude characters of this user, including anonymous if 0.
 *  @param $string
 *  A partial string to match.
 */
function fsgame_autocomplete_exclude($uid = NULL, $string = '') {
  $matches = array();

  $nodes = _fsgame_potential_nodes(NULL, TRUE, $string, FALSE, $uid);
  foreach ($nodes as $node) {
    $matches[check_plain($node->title) .' ('. $node->name .') ['. $node->nid .']'] = check_plain($node->title) .' ('. $node->name .') ['. $node->nid .']';
  }
  drupal_json($matches);
}

/**
 * Fetch an array of all candidate character nodes,
 * for use in presenting the selection form to the user.
 * @TODO: Allow views to further determine fetched characters, such as a from a specific guild. See nodereference.
 */
function _fsgame_potential_nodes($uid = 0, $return_full_nodes = FALSE, $string = '', $exact_string = FALSE, $exclude_uid = NULL) {
  // standard field : referenceable nodes defined by content types
  // build the appropriate query
  $conditions = array(" n.type = '%s'");
  $args = array('fsgame_character');

  if ($uid && is_numeric($uid)) {
    $conditions[] = " n.uid = %d";
    $args[] = $uid;
  }

  if (isset($string)) {
    $conditions[] = $exact_string ? " n.title = '%s'" : " n.title LIKE '%%%s%'";
    $args[] = $string;
  }

  if (isset($exclude_uid) && is_numeric($exclude_uid)) {
    $conditions[] = " n.uid <> %d";
    $args[] = $exclude_uid;
  }

  $conditions_clause = implode(' AND', $conditions);

  $result = db_query(db_rewrite_sql("SELECT n.nid, n.title, u.name FROM {node} n INNER JOIN {users} u ON u.uid = n.uid WHERE ". $conditions_clause ." ORDER BY n.title, n.type"), $args);

  $rows = array();

  while ($node = db_fetch_object($result)) {
    if ($return_full_nodes) {
      $rows[$node->nid] = $node;
    }
    else {
      $rows[$node->nid] = $node->node_title;
    }
  }

  return $rows;
}

function _fsgame_autocomplete_nid($character) {
  if ($character && is_numeric($character)) {
    $nid = $character;
  }
  else {
    preg_match('@\[([0-9]+)\]@', $character, $matches);
    $nid = $matches[1];
  }
  return $nid;
}

function fsgame_character_claim($node) {
  if (!fsgame_claim_access($node)) {
    drupal_access_denied();
  }
  global $user;
  $node->uid = $user->uid;
  node_save($node);
  drupal_set_message(t("You have claimed the character %title as your own.", array('%title' => $node->title)));
  unset($_SESSION['fsgame_contest_anonymous_character']);
  drupal_goto('node/'. $node->nid);
}
