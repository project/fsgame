<?php

/**
 * @file
 * Admin functions for the 5 Second Game engine.
 */

/**
 * Configure 5 Second Game; menu callback for admin/settings/fsgame.
 */
function fsgame_settings() {
  $form = array();

  if (!count(fsgame_world_load('classes', NULL, TRUE))) {
    drupal_set_message(t("No character classes found. !enable_a_module that defines a 5 Second Game world.",
      array('!enable_a_module' => l(t('Enable a module'), 'admin/build/modules'))), 'error');
  }

  $form['attributes'] = array(
    '#collapsible'      => TRUE,
    '#description'      => t('All characters have three base attributes, and the engine resolves conflicts using a simple rock-paper-scissors comparison, where rock beats scissors beats paper. You may change the names and definitions of these attributes here. When using modules that add classes, skills, and modifiers such as equipment, the attributes used in those modules will be translated to the terms you\'ve defined here.'),
    '#title'            => t('Attributes'),
    '#type'             => 'fieldset',
  );
  foreach (fsgame_base_attributes() as $attribute) {
    $form['attributes'][$attribute['id']] = array(
      '#collapsible'    => TRUE,
      '#collapsed'      => TRUE,
      '#title'          => t(drupal_ucfirst($attribute['id'])),
      '#type'           => 'fieldset',
    );
    $form['attributes'][$attribute['id']]['fsgame_attributes_'. $attribute['id'] .'_title'] = array(
      '#default_value'  => variable_get('fsgame_attributes_'. $attribute['id'] .'_title', t(drupal_ucfirst($attribute['title']))),
      '#description'    => t("The name shown to users of the internal '@attribute' attribute.", array('@attribute' => $attribute['id'])),
      '#title'          => t('Title'),
      '#type'           => 'textfield',
    );
    $form['attributes'][$attribute['id']]['fsgame_attributes_'. $attribute['id'] .'_description'] = array(
      '#default_value'  => variable_get('fsgame_attributes_'. $attribute['id'] .'_description', ''),
      '#description'    => t("The description shown to users of the internal '@attribute' attribute.", array('@attribute' => $attribute['id'])),
      '#title'          => t('Description'),
      '#type'           => 'textfield',
    );
    $form['attributes'][$attribute['id']]['fsgame_attributes_'. $attribute['id'] .'_default'] = array(
      '#default_value'  => variable_get('fsgame_attributes_'. $attribute['id'] .'_default', 1),
      '#description'    => t("The default value of the '@attribute' attribute for starting characters.", array('@attribute' => $attribute['id'])),
      '#title'          => t('Default value'),
      '#type'           => 'textfield',
    );
    $form['attributes'][$attribute['id']]['fsgame_attributes_'. $attribute['id'] .'_minimum'] = array(
      '#default_value'  => variable_get('fsgame_attributes_'. $attribute['id'] .'_minimum', 0),
      '#description'    => t("The minimum value of the '@attribute' attribute.", array('@attribute' => $attribute['id'])),
      '#title'          => t('Minimum value'),
      '#type'           => 'textfield',
    );
    $form['attributes'][$attribute['id']]['fsgame_attributes_'. $attribute['id'] .'_maximum'] = array(
      '#default_value'  => variable_get('fsgame_attributes_'. $attribute['id'] .'_maximum', 0),
      '#description'    => t("The maximum value of the '@attribute' attribute. If set to 0 (the default), there is no upper limit.", array('@attribute' => $attribute['id'])),
      '#title'          => t('Maximum value'),
      '#type'           => 'textfield',
    );
    $form['attributes'][$attribute['id']]['fsgame_attributes_'. $attribute['id'] .'_trumps'] = array(
      '#default_value'  => variable_get('fsgame_attributes_'. $attribute['id'] .'_trumps', $attribute['trumps']),
      '#description'    => t("When resolving ties, the '@attribute' attribute is weighted higher than the attribute selected here.", array('@attribute' => $attribute['id'])),
      '#options'        => fsgame_base_attributes_fapi(),
      '#title'          => t('Trumps'),
      '#type'           => 'select',
    );
  }

  $form['#validate'][] = 'fsgame_settings_validate';
  return system_settings_form($form);
}

/**
 * FormAPI #validate callback for fsgame_settings().
 */
function fsgame_settings_validate($form, &$form_state) {
  foreach (fsgame_base_attributes() as $attribute) {
    if ($form_state['values']['fsgame_attributes_' . $attribute['id'] . '_trumps'] == $attribute['id']) {
      form_set_error('fsgame_attributes_' . $attribute['id'] . '_trumps', t("The '@attribute' attribute can not trump itself.", array('@attribute' => $attribute['id'])));
    }
  }
}
