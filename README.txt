
CONTENTS OF THIS FILE
---------------------

 * Introduction and Initial Design Goals
 * Installation
 * The Three Attributes
 * How Combat Works
 * Available Hooks


INTRODUCTION AND INITIAL DESIGN GOALS
=====================================

The engine for 5 Second Game is built on a rock-paper-scissors concept. There
are three stats, corresponding to each element, and everything modifies those
values: character classes, skills, combat, etc. 5 Second Game is meant to be
a fast paced, easy to grasp, infinitely extensible game.

The game is designed to be fit into an embeddable widget, so that users may
post the game in a blog or other page, allowing others to quickly and easily
play. A widget may highlight that user's character, challenging all takers.
Anonymous battles may also be possible.

The game lends itself well to guilds and the like as well, which are also
built into the system. Characters may belong to one or more guilds, which
govern themselves as desired by their players. The system honors the guilds'
wishes, so that characters belonging to a specific user-formed guild might
gain certain bonuses (or suffer penalties) according to the guild charters.

Additionally, servers may choose to allow characters to migrate between
certain other servers. The character may be "translated" during migration,
so that world/genre integrity is maintained (if desired). However, the
simplicity of the system lends itself easily to either scenario, if an admin
doesn't mind a gun-toting sheriff to arrive at the scene of a medieval
tournament, or conversely if the player of a high elf wizard doesn't mind
being translated to a nanobotic engineer on a space station.

You can play the game at http://5secondgame.com to see it in action.


INSTALLATION
============

1. Copy the files to your sites/SITENAME/modules directory.
   Or, alternatively, to your sites/all/modules directory.

2. Enable the 5 Second Game modules at admin/build/modules. Be sure
   to enable a "World" module too - the game won't work without one.

3. You can configure 5 Second Game at admin/settings/fsgame.

4. To create a character, head to node/add/fsgame-character.

5. To enter into a contest with someone else...


THE THREE ATTRIBUTES
====================

The three base attributes, at their root, are Rock, Paper, and Scissors. As
in the schoolyard game of that name, Rock beats Scissors beats Paper. The
attributes can be renamed at the administrator's pleasure; for instance, at
http://5secondgame.com, the stats are named Spirit, Mind, and Body. Thus,
Spirit trumps Mind, which trumps Body, which trumps Spirit.

Characters will have a score in each of the attributes, beginning at 1 and
going infinitely beyond (although on a practical level, the administrator
will usually top out the attributes at a managable level, and/or force
the retirement of overly powerful characters).

When competing, two characters will choose one of the three attributes (by
choosing a particular skill that uses that attribute), and compare them. The
chosen attribute will be modified according to character class, skill levels
of the chosen form of competition, and other effects and circumstances. The
final results are compared, and a winner is chosen.


HOW COMBAT WORKS
================


AVAILABLE HOOKS
===============

