<?php

function theme_test($file = NULL) {
  drupal_set_message("$file is ". print_r($file, TRUE));
  return $file['filepath'];
}

/**
 * @file
 * Theme functions for the 5 Second Game Engine.
 */

/**
 *  Display all attributes for a character.
 */
function theme_fsgame_character_attributes($node) {
  $output = '<div class="fsgame-character-attributes">';
  foreach (fsgame_base_attributes() as $attribute) {
    $output .= '<div class="fsgame-character-attribute fsgame-character-attribute-' . $attribute['id'] . '">';
    $output .=   '<div class="fsgame-character-attribute-label">' . $attribute['title'] . '</div>';
    $output .=   '<div class="fsgame-character-attribute-value">' . $node->fsgame['character'][$attribute['id']] . '</div>';
    $output .= '</div>';
  }
  $output .= '</div>';
  return $output;
}

/**
 *  Display the character's class.
 */
function theme_fsgame_character_class($node) {
  $class = fsgame_world_load('classes', $node->fsgame['character']['class']);

  if (!$class) {
    drupal_set_message("This character's class was not found; please contact a site administrator.", 'error');
    return; // if this character no longer is plausible, complain loudly to the site admin cos they broke something.
  }

  $output .= '<div class="fsgame-character-class">';
  $output .= t('Level @level !class_and_modifiers.',
    array(
      '@level' => $node->fsgame['character']['level'],
      '!class_and_modifiers' => fsgame_attributes_modifiers_string($class),
  ));
  $output .= '</div>';
  return $output;
}

/**
 *  Display a list of contest character links.
 */
function theme_fsgame_contest_selectors_character($character_nid = 'recent', $opponent_nid = 'recent') {
  $output .= 'Or select one of the following:';
  $items = array(
    l(t('Random character'), 'fsgame/contest/random/'. $opponent_nid),
    l(t('Create new character'), 'node/add/fsgame-character', array('query' => 'destination=fsgame/contest/latest/'. $opponent_nid)),
  );
  $output .= theme('item_list', $items);
  return $output;
}

/**
 *  Display a list of contest opponent links.
 */
function theme_fsgame_contest_selectors_opponent($character_nid = 'recent', $opponent_nid = 'recent') {
  $output .= 'Or select one of the following:';
  $items = array(
    l(t('Random opponent'), 'fsgame/contest/'. $character_nid .'/random'),
  );
  $output .= theme('item_list', $items);
  return $output;
}

